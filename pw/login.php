<?php
	session_start();
	require '../config/db.php';
	require 'header.php';
	if(!empty($_SESSION['username'])){
		header("Location:index.php");
		exit();
	}
	if((isset($_POST['user_conf']))&& !empty($_POST['user_conf']))
	{
		try
		{
			$user= htmlspecialchars($_POST['user']);
			$user_pass= htmlspecialchars($_POST['user_pass']);
			$conn= new PDO("mysql:host=$dbhost;dbname=spelections;charset:utf8",$dbuser,$dbpass);
			$conn-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$conn-> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			$stmt= $conn->prepare("SELECT * FROM authenticate WHERE username= :Username");
			$stmt-> bindParam(":Username",$user);
			$stmt-> execute();
			$result= $stmt->fetch(PDO::FETCH_ASSOC);
			if($result)
			{
//				echo $user_pass;echo $result['Password'];
				if(password_verify($user_pass,$result['hashed']))
				{
					session_start();
					$_SESSION['username'] =$user;
					$_SESSION['hostel']= $result['hostel'];
					header('Location:index.php');
				}
				else
				{
					?><script>alert("Invalid Password");</script><?php
				}
			}
			else
			{
				?><script>alert("Enter the right credentials");</script><?php
			}
		}
		catch(PDOException $e)
		{
			echo $e;
		}
	}
	$title= "Warden Login";
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $title;?></title>
		<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-15'>
		<link rel='shortcut icon' href='../favicon.ico' type='image/x-icon'>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	</head>	
	<body>
		<div class="container-fluid">
			<div class= 'col-xs-8 col-sm-8 col-md-8 col-lg-8 col-lg-offset-2'>
				<img src="images/iitmadras.png" height="150" width="150" class="center-block" alt="Responsive Image">
			</div>
			<div class= 'col-xs-8 col-sm-8 col-md-8 col-lg-8 col-lg-offset-2'>
				<h2 class="text-center">Welcome to IIT Madras Elections Portal</h2>
				<h3 class="text-center">Warden Login</h3>
			</div>
			<form action='' method='POST' id='user' name='user' enctype="multipart/form-data">
				<div class= "col-xs-8 col-sm-8 col-md-8 col-lg-8 col-lg-offset-2">
					<div class="form-group">
						<label for="user">Username</label>
						<input type="text" class="form-control" id="user" name="user" placeholder="Username" />
					</div>
				</div>
				<div class= "col-xs-8 col-sm-8 col-md-8 col-lg-8 col-lg-offset-2">			
					<div class="form-group">
						<label for="user_pass">Password</label>
						<input type="password" class="form-control" id="user_pass" name="user_pass" placeholder="Password" />
					</div>
				</div>
				<div class= "col-xs-8 col-sm-8 col-md-8 col-lg-8 col-lg-offset-5">			
					<input type="submit" class="btn btn-lg btn-primary" id="user_conf" name="user_conf" style="margin-top:25px;"></input>				
				</div>
			</form>
		</div>
	</body>
</html>
