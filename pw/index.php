<?php
echo "<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-15'>
<link rel='shortcut icon' href='../favicon.ico' type='image/x-icon'></head>";
$error_message='';
require '../config/db.php';

$remote_addr = $_SERVER[ 'REMOTE_ADDR' ];
//$remote_addr = '10.21.203.54';

session_start();
require 'header.php';
if(empty($_SESSION['username']))
{
	$_SESSION['Error']= 'Please Login';		
	header('Location:login.php');
	exit();		
}
$hostel= $_SESSION['hostel'];
//$hostel_admin_id = 22;

if($_POST && isset($_POST['roll_number'])) {
$roll_number = $_POST['roll_number'];
$roll_number = strtoupper( $roll_number );
$roll_number = mysql_real_escape_string( $roll_number );
 try
    {

			$conn= new PDO("mysql:host=$dbhost;dbname=spelections;charset:utf8",$dbuser,$dbpass);
			$conn-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$conn-> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			$stmt= $conn->prepare("SELECT * FROM voters WHERE roll_no=:roll");
			$stmt-> bindParam(":roll",$roll_number);
			$stmt-> execute();
			$result= $stmt->fetch(PDO::FETCH_ASSOC);
			if($result){
					if($hostel==$result['hostel']){
						$password = $result['password'];
					?>
	<style>
	#alert{
	  margin: 0;
	  padding:0;
	  z-index: 9999;
	  position: fixed;
	  height: 100%;
	  width: 100%;
	  background: #FFF;
	}
	.alert-content{
	  width:300px;
	  margin:auto;
	  margin-top:100px;
	  font-size:30px;
	}
	.alert-content button {
	  padding:10px 40px;
	  font-size:20px;
	  margin:0 0 0 50px;
	}
	</style>
	<script>
	function close_alert() {
		document.getElementById("alert").remove();
	};
	</script>
	<div id='alert'><div class='alert-content'><p><?=$roll_number?> - <?=$password?></p><br><br> <button onClick='close_alert()'>Close</button></div></div>
	
<?php
}
else
{
	$error_message='Invalid Username';
}
}
else{
	$error_message = 'Invalid Username';
}
}
catch(PDOException $e){
      echo $e;
}
}
?>
<!-- Set focus on roll number input box -->
<?php $params_body = "onLoad = 'set_focus()'"; ?>

<!-- Load the header -->
<?php require_once('../header.php');?>

<!-- Get session message if set and unset it -->
<?php
if( isset($_SESSION[ 'message' ]) && $_SESSION['message'] )
  {
    $error_message = $_SESSION[ 'message' ];
    unset( $_SESSION[ 'message' ] );
  }
?>

<!-- Display an error message, if any -->
<p class="error_message">
  <?php echo $error_message; ?>
</p>
<link rel="stylesheet" href="../css/style.css" type="text/css">
<!-- Show the login form -->
<form class="login" method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
<h2>Roll Number</h2>
<input type="text" class="text" size="32" name="roll_number" id="roll_number" />
<p>Lowercase/uppercase does not matter (case-insensitive)</p><br>
<input type="submit" class="button" value="Click here to proceed" />
</form>
<a href="logout.php">Log out</a>
<!-- Load the footer -->
<?php require_once('../footer.php'); ?>
</html>
