<?php
/*** PHP code to process the login form ***/
$error_message='';

require_once('config/db.php');

$remote_addr = $_SERVER[ 'REMOTE_ADDR' ];
//$ds_ip = '10.22.32.101';
//$ds_ip_2= '10.22.19.219';
/*** Activate sessions ***/
session_start();
require_once('header.php');
if(!empty($_SESSION['roll_number']))
{
	header("Location:vote.php");
	exit();
}
/*** Page variables ***/
//$vote_screen = "vote.php"; // Where the login should redirect to.
$error_message="";
$_SESSION['message']="";
/*** Ensure that the user is not already logged in ***/
/*if( $_SESSION[ 'auth' ] )
>>>>>>> 03a6354fb17f2cffb3a21f21d4fde423d070a279
{
  $error_message = $_SESSION[ 'message' ]; 
  $_SESSION = array();
  session_destroy();
}
*/
/*** Get values 
	We must check the following:
	* escape strings
	* password matches
	* correct hostel
	* hasn't voted yet
***/
if($_POST && isset($_POST['roll_number']))
{
    $roll_number = $_POST['roll_number'];
    $password = $_POST['password'];

    /*** SQL Injection Protection: Verify that 'roll_number' is a valid string 
     * (i.e. 8 chars) - after mysql_real_escape_string ***/
    $roll_number = strtoupper( $roll_number );
    $roll_number = mysql_real_escape_string( $roll_number );
//   $password = strtolower( $password );
    $password = mysql_real_escape_string( $password );


    //if( preg_match( "/^\w{8}$/", $roll_number, $matches ) && 
//        $result = mysql_query( "SELECT `password`, `hostel_id`, `voted`, `name` FROM `voter` WHERE `roll_number`='$matches[0]'" ) )
	try
	{
		$conn= new PDO("mysql:host=$dbhost;dbname=spelections;charset:utf8",$dbuser,$dbpass);
    $conn-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn-> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $stmt= $conn->prepare("SELECT * FROM voters WHERE roll_no=:roll");
    $stmt-> bindParam(":roll",$roll_number);
    $stmt-> execute();
    $result= $stmt->fetch(PDO::FETCH_ASSOC);
    if($result)
  	{
				$voter_name = $result[ 'name' ];
        $voter_hostel= $result['hostel'];
	// Check password
	// Check if he hasn't already voted
        if( password_verify($password,$result['hashedpwd']) && $result['voted'] == 0 )
        {
            /*** Ensure that voting for this hostel is still open ***/
			/*			$stmt1= $conn->prepare("SELECT * FROM voters WHERE roll_no=:roll");
    				$stmt1-> bindParam(":roll",$roll_number);
   			 		$stmt1-> execute();
    				$result1= $stmt1->fetch(PDO::FETCH_ASSOC);
            $query = "SELECT `status` FROM `hostel` WHERE `id`='".$row['hostel_id']."'";
            $result_ = mysql_query( $query );
            $row_ = mysql_fetch_assoc( $result_ );
            if( !$row_[ 'status' ] )
            {
                $error_message = "Voting is currently closed";
            }
            else
            {*/
                /*** Now, verify that the user is voting from the right hostel. ***/
                $query= "SELECT `id` FROM `hostel` WHERE `name`='".$voter_hostel."'";
                $id= mysql_query($query);
                $query = "SELECT `ip` FROM `hostel_ip` WHERE `hostel_id`='".$id."'";
                $result_ = mysql_query( $query );
                assert( $result_ );
                if($remote_addr==$ds_ip || $remote_addr==$ds_ip_2)
                {
                  $_SESSION['roll_number'] = $roll_number;
                  $_SESSION['message'] = "Login successful";
                  $_SESSION['voter_name'] = $voter_name;
                  header( "Location: vote2.php" ); 
                }
                while( $row_ = mysql_fetch_assoc( $result_ ) )
                {
                    if( $remote_addr == $row_[ 'ip' ] )
                    {
                        /*** Set session variables confirming that the user is 
                         * authenticated ***/

                     //   $_SESSION[ 'auth' ] = true;
                        $_SESSION['roll_number'] = $roll_number;
                        $_SESSION['message'] = "Login successful";
											  $_SESSION['voter_name'] = $voter_name;
                      	header( "Location: vote2.php" );
                    }
                }
                $_SESSION['message']= "Vote from your own hostel";
          //          }
          //      }
          //      $error_message = "Please vote in your own hostel.";
          //  }
        }
        else if(password_verify($password,$result['password']) && $result['voted'] == 1)
        {
            $_SESSION[ 'message' ] = "You have already voted";    
        }
    }
    /*** If you reach here, that means something went wrong with your login ***/
    if( !$error_message ) $error_message = "Invalid username/password";
	}
		catch(PDOException $e){
      echo $e;
		}
}

/*** End of PHP code to process the login form ***/
?>

<!-- Set focus on roll number input box -->
<?php $params_body = "onLoad = 'set_focus()'"; ?>

<!-- Load the header -->

<!-- Get session message if set and unset it -->
<?php
if(!empty($_SESSION[ 'message' ])) 
  {
    $error_message = $_SESSION[ 'message' ];
  }
?>

<!-- Display an error message, if any -->
<p class="error_message">
  <?php echo $error_message; ?>
</p>

<!-- Show the login form -->
<form class="login" method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
<h2>Roll Number</h2>
<input type="text" class="text" size="32" name="roll_number" id="roll_number" />
<p>Lowercase/uppercase does not matter (case-insensitive)</p><br>
<h2>Password</h2>
<input type="password" class="text" size="32" name="password" />
<p>Enter the password exactly as given in the sticker</p>
<input type="submit" class="button" value="Click here to proceed" />
</form>

<!-- Load the footer -->
<?php require_once('footer.php'); ?>
