<?php
session_start();
if(empty($_SESSION['roll_number']))
{
header("Location:index.php");
exit();
}
else{
echo "<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-15'>
<link rel='shortcut icon' href='favicon.ico' type='image/x-icon'>
<link rel='stylesheet' href='css/bootstrap.min.css'>
<link rel='stylesheet' href='css/votepage.css'>
<title>Institute &amp; Hostel Council Elections 2015</title>
<script src='js/bootstrap.min.js'></script></head><body>";
require_once('config/db.php');
require_once('header.php');
$roll_number=$_SESSION['roll_number'];
try
{
	$conn= new PDO("mysql:host=$dbhost;dbname=spelections;charset:utf8",$dbuser,$dbpass);
	$conn-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn-> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$stmt= $conn->prepare("SELECT * FROM voters WHERE roll_no=:roll");
	$stmt-> bindParam(":roll",$roll_number);
	$stmt-> execute();
	$result= $stmt->fetch(PDO::FETCH_ASSOC);
	if($result["voted"]==0)
	{
		$hostel=$result["hostel"];
		$user_id=$result["id"];
		try
		{?>
			<form class="login" method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
			<?php
			$count=1;
			echo "<h1>".$hostel." Hostel Elections</h1>";
			while($count<5)
			{	
				$stmt1= $conn->prepare("SELECT * FROM candidates WHERE hostel=:hostel and position_id=:count");
				$stmt1-> bindParam(":hostel",$hostel);
				$stmt1-> bindParam(":count",$count);
				$stmt1-> execute();?>
				<?php
				try
				{
					$stmt2= $conn->prepare("SELECT * FROM position WHERE id=:id");
					$stmt2-> bindParam(":id",$count);
					$stmt2-> execute();
					$result2=$stmt2->fetch(PDO::FETCH_ASSOC);
					$position=$result2["position_name"];
				}
				catch(PDOException $e)
				{
					echo $e;
				}
				if($hostel=="Pampa" && $position=="Technical_Affairs_Secretary")
					echo "<div class='row'><h3>".$position."</h3>";
				else if($hostel=="Mahanadhi")
					echo "<div class='row'><h3>".$position."</h3>";
				while($result1=$stmt1->fetch(PDO::FETCH_ASSOC))
				{
					if($result1["name"]=="Abstain")
					{
						echo "<div class='col-md-2'><img src='images/abstain.jpg' height='120' width='120'><br><div class='abstain'><input type='radio' name=".$position." value=".$result1["id"]." checked>".$result1["name"]."</div></div>";
					}
					else
					{
						echo "<div class='col-md-2'><img src='images/pic.jpg' alt='".$result1["name"]."'>";
						echo "<br><input type='radio' name=".$position." value=".$result1["id"].">".$result1["name"]."</div>";
					}
				}
				echo "</div>";
				$count++;
			}
			echo "<br><input type='submit' value='Submit'></form>";
		}
		catch(PDOException $e)
		{
			echo $e;
		}
		if($_POST)
		{
			$var=false;
			$count=1;
			while($count<5)
			{
				try
				{
					$stmt3= $conn->prepare("SELECT * FROM position WHERE id=:id");
					$stmt3-> bindParam(":id",$count);
					$stmt3-> execute();
					$result3=$stmt3->fetch(PDO::FETCH_ASSOC);
					$position_name=$result3["position_name"];
				}
				catch(PDOException $e)
				{
					echo $e;
				}
				$candidate_number=$_POST[$position_name];
				try
				{
					$stmt4= $conn->prepare("INSERT INTO votes(user_id,voted_to) VALUES (:user_id,:voted_to)");
					$stmt4-> bindParam(":user_id",$user_id);
					$stmt4-> bindParam(":voted_to",$candidate_number);
					$stmt4-> execute();
				}
				catch(PDOException $e)
				{
					echo $e;
				}
				$count++;
				try
				{
					$stmt6= $conn->prepare("SELECT * FROM candidates WHERE id=:psnid");
					$stmt6-> bindParam(":psnid",$candidate_number);
					$stmt6-> execute();
					$result6=$stmt6->fetch(PDO::FETCH_ASSOC);
					$votecount=$result6["count"];
					try
					{
						$votecount++;
						$stmt7= $conn->prepare("UPDATE candidates SET count=:votecount WHERE id=:psnid");
						$stmt7-> bindParam(":psnid",$candidate_number);
						$stmt7-> bindParam(":votecount",$votecount);
						$stmt7-> execute();
					}
					catch(PDOException $e)
					{
						echo $e;
					}
				}
				catch(PDOException $e)
				{
					echo $e;
				}
			}
			try
			{
				$voted=1;
				$stmt5= $conn->prepare("UPDATE voters SET voted=:voted WHERE roll_no=:roll");
  			$stmt5-> bindParam(":roll",$roll_number);
				$stmt5-> bindParam(":voted",$voted);
				$stmt5-> execute();
			}
			catch(PDOException $e)
			{
				echo $e;
			}
			session_unset();
			session_destroy();
			$var=true;
			if($var)
			{
				header("Location:index.php");
				exit();
			}
		}
	}
	else
	{
		header("Location:index.php");
	}
}
catch(PDOException $e)
{
	echo $e;
}
echo "</body></html>";
}
?>
